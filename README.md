# Evaluacion Bankapp

El proyecto está basado en targetSdk 32. Con campatibilidad desde la API 21. Todo sobre Java 1.8 y clases en Java.

Se utilizó Android Studio Chipmunk | 2021.2.1 Patch 1

### Librerías usadas
- Retrofit v2.9.0
- Gson converter v2.6.4

### Ejecucion del proyecto
Basta con abrir el proyecto con Android Studio y ejecutar el gradle para descargar las librerías.
Posteriormente correr la app en emulador o dispositivo físico.
