package com.example.evaluacionbankapp.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.evaluacionbankapp.R;
import com.example.evaluacionbankapp.adapters.ItemCardAdapter;
import com.example.evaluacionbankapp.adapters.ItemMovementAdapter;
import com.example.evaluacionbankapp.components.ToolBar;
import com.example.evaluacionbankapp.components.UnderlinedTextView;
import com.example.evaluacionbankapp.controllers.MainActivityController;
import com.example.evaluacionbankapp.models.CuentaResponseModel;
import com.example.evaluacionbankapp.models.Cuentum;
import com.example.evaluacionbankapp.models.MovementsResponseModel;
import com.example.evaluacionbankapp.models.Movimiento;
import com.example.evaluacionbankapp.models.Saldo;
import com.example.evaluacionbankapp.models.SaldosResponseModel;
import com.example.evaluacionbankapp.models.Tarjeta;
import com.example.evaluacionbankapp.models.TarjetasResponseModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {

    private MainActivityController myMainActivityController;
    private ToolBar toolbar;
    private UnderlinedTextView utvMyAccount;
    private UnderlinedTextView utvSendMoney;
    private UnderlinedTextView utvAddCard;
    private TextView tvAccountName;
    private TextView tvLastDate;
    private RecyclerView rvTarjetas;
    private ItemCardAdapter itemCardAdapter;
    private ItemMovementAdapter itemMovementAdapter;
    private List<TextView> titleSaldos = new ArrayList<>();
    private List<TextView> numberSaldos = new ArrayList<>();
    private List<Movimiento> movimientos = new ArrayList<>();
    private RecyclerView rvMovements;
    private TextView tvShowAllMovements;
    private Boolean isMovementsOpened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myMainActivityController = new MainActivityController();
        myMainActivityController.addObserver(this);

        utvMyAccount = findViewById(R.id.utv_my_account);
        utvSendMoney = findViewById(R.id.utv_send_money);
        utvAddCard = findViewById(R.id.utv_add_card);

        tvAccountName = findViewById(R.id.tv_account_name);
        tvLastDate = findViewById(R.id.tv_last_date);

        for (int i = 1; i <= 3; i++) {
            TextView textView = findViewById(getResources().getIdentifier("tv_" + i, "id", getPackageName()));
            TextView textView2 = findViewById(getResources().getIdentifier("tv_saldo_" + i, "id", getPackageName()));
            titleSaldos.add(textView);
            numberSaldos.add(textView2);
        }

        rvTarjetas = findViewById(R.id.rv_tarjetas);
        itemCardAdapter = new ItemCardAdapter(this, new ArrayList<>());
        rvTarjetas.setAdapter(itemCardAdapter);

        rvMovements = findViewById(R.id.rv_movements);
        itemMovementAdapter = new ItemMovementAdapter(this, movimientos, false);
        rvMovements.setAdapter(itemMovementAdapter);

        tvShowAllMovements = findViewById(R.id.tv_show_all_movements);

        utvMyAccount.setOnClickListener(view -> {

        });

        utvSendMoney.setOnClickListener(view -> {

        });

        utvAddCard.setOnClickListener(view -> {
            Intent intent = new Intent(this, AddCardActivity.class);
            startActivity(intent);
        });

        tvShowAllMovements.setOnClickListener(view -> {
            if (!isMovementsOpened) {
                tvShowAllMovements.setText("Cerrar Todos Los Movimientos");
                itemMovementAdapter.updateMovements(movimientos, false);
                isMovementsOpened = true;
            } else {
                tvShowAllMovements.setText("Ver Todos Los Movimientos");
                itemMovementAdapter.updateMovements(movimientos, true);
                isMovementsOpened = false;
            }
        });

    }

    @Override
    public void update(Observable observable, Object o) {
        CuentaResponseModel accountInfo = myMainActivityController.getAccountInfo();
        SaldosResponseModel saldosResponseModel = myMainActivityController.getSaldos();
        TarjetasResponseModel tarjetasResponseModel = myMainActivityController.getTarjetas();
        MovementsResponseModel movimientosResponseModel = myMainActivityController.getMovimientos();
        if (accountInfo != null) {
            Cuentum cuenta = accountInfo.getCuenta().get(0);
            tvAccountName.setText(cuenta.getNombre());
            tvLastDate.setText(cuenta.getUltimaSesion());
        }
        if (saldosResponseModel != null) {
            String numberFormatted = "";
            for (int i = 1; i <= 3; i++) {
                Saldo saldo = saldosResponseModel.getSaldos().get(0);
                switch (i) {
                    case 1:
                        titleSaldos.get(0).setText("Saldo general\nen cuentas");
                        numberFormatted = NumberFormat.getInstance().format(saldo.getSaldoGeneral());
                        numberSaldos.get(0).setText(String.format("$%s.00", numberFormatted));
                        break;
                    case 2:
                        titleSaldos.get(1).setText("Total de ingresos\n");
                        numberFormatted = NumberFormat.getInstance().format(saldo.getIngresos());
                        numberSaldos.get(1).setText(String.format("$%s.00", numberFormatted));
                        break;
                    case 3:
                        titleSaldos.get(2).setText("Total de gastos\n");
                        numberFormatted = NumberFormat.getInstance().format(saldo.getGastos());
                        numberSaldos.get(2).setText(String.format("$%s.00", numberFormatted));
                        break;
                }
            }
        }
        if (tarjetasResponseModel != null) {
            System.out.println("tarjetasResponseModel");
            System.out.println(tarjetasResponseModel.getTarjetas());
            itemCardAdapter = new ItemCardAdapter(getApplicationContext(), tarjetasResponseModel.getTarjetas());
            rvTarjetas.setAdapter(itemCardAdapter);
        }if (movimientosResponseModel != null) {
            System.out.println("movimientosResponseModel");
            System.out.println(movimientosResponseModel.getMovimientos());
            movimientos = movimientosResponseModel.getMovimientos();
            itemMovementAdapter = new ItemMovementAdapter(getApplicationContext(), movimientos, false);
            rvMovements.setAdapter(itemMovementAdapter);
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
        Log.i("onPointerCaptureChanged", hasCapture + "");
    }
}