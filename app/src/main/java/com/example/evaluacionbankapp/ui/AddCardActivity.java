package com.example.evaluacionbankapp.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.example.evaluacionbankapp.R;
import com.example.evaluacionbankapp.components.CustomTextInputLayout;
import com.example.evaluacionbankapp.components.ToolBar;
import com.example.evaluacionbankapp.providers.OutlineProvider;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONObject;

import java.util.Objects;

public class AddCardActivity extends AppCompatActivity {

    private ToolBar toolBar;
    private ImageView ivCardPlaceholder;
    private AppCompatButton btnCancel;
    private AppCompatButton btnAdd;
    private CustomTextInputLayout ilCardNumber;
    private CustomTextInputLayout ilCardName;
    private CustomTextInputLayout ilAccount;
    private CustomTextInputLayout ilIssure;
    private CustomTextInputLayout ilType;
    private CustomTextInputLayout ilStatus;
    private CustomTextInputLayout ilAmount;
    private CustomTextInputLayout ilAccountType;
    JSONObject json = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        toolBar = findViewById(R.id.toolbar);
        toolBar.setOnClickListener(view -> {
            onBackPressed();
        });

        ivCardPlaceholder = findViewById(R.id.iv_card_placeholder);
        ivCardPlaceholder.setOutlineProvider(new OutlineProvider());

        btnCancel = findViewById(R.id.btn_cancel);
        btnAdd = findViewById(R.id.btn_add);

        ilCardNumber = findViewById(R.id.ctil_card_number);
        ilCardName = findViewById(R.id.ctil_card_name);
        ilAccount = findViewById(R.id.ctil_account);
        ilAccountType = findViewById(R.id.ctil_account_type);
        ilAmount = findViewById(R.id.ctil_amount);
        ilIssure = findViewById(R.id.ctil_issure);
        ilStatus = findViewById(R.id.ctil_status);
        ilType = findViewById(R.id.ctil_card_type);

        btnAdd.setOnClickListener(view -> {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this, R.style.AlertDialogCustom);
            if (json.length() > 0)
                json = new JSONObject();
            try {
                json.put("card_number", getTextInputLayout(ilCardNumber));
                json.put("card_name", getTextInputLayout(ilCardName));
                json.put("account", getTextInputLayout(ilAccount));
                json.put("account_type", getTextInputLayout(ilAccountType));
                json.put("amount", getTextInputLayout(ilAmount));
                json.put("issure", getTextInputLayout(ilIssure));
                json.put("status", getTextInputLayout(ilStatus));
                json.put("card_type", getTextInputLayout(ilType));
                dialogBuilder.setTitle("Datos para agregar tarjeta");
                dialogBuilder.setMessage(json.toString());
                dialogBuilder.setPositiveButton("Cerrar", (dialogInterface, i) -> dialogInterface.dismiss());
                dialogBuilder.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        btnCancel.setOnClickListener(view -> {
            onBackPressed();
        });
    }

    private String getTextInputLayout(CustomTextInputLayout layout) {
        TextInputEditText editText = layout.getEditText();
        if (editText != null) {
            return Objects.requireNonNull(editText.getText()).toString().trim();
        }
        return "";
    }
}