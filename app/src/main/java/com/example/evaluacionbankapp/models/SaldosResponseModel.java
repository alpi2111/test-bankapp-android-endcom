
package com.example.evaluacionbankapp.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SaldosResponseModel {

    @SerializedName("saldos")
    private List<Saldo> mSaldos;

    public List<Saldo> getSaldos() {
        return mSaldos;
    }

    public void setSaldos(List<Saldo> saldos) {
        mSaldos = saldos;
    }

}
