
package com.example.evaluacionbankapp.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CuentaResponseModel {

    @SerializedName("cuenta")
    private List<Cuentum> mCuenta;

    public List<Cuentum> getCuenta() {
        return mCuenta;
    }

    public void setCuenta(List<Cuentum> cuenta) {
        mCuenta = cuenta;
    }

}
