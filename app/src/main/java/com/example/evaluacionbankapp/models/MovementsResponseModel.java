
package com.example.evaluacionbankapp.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MovementsResponseModel {

    @SerializedName("movimientos")
    private List<Movimiento> mMovimientos;

    public List<Movimiento> getMovimientos() {
        return mMovimientos;
    }

    public void setMovimientos(List<Movimiento> movimientos) {
        mMovimientos = movimientos;
    }

}
