
package com.example.evaluacionbankapp.models;

import com.google.gson.annotations.SerializedName;

public class Saldo {

    @SerializedName("cuenta")
    private Long mCuenta;
    @SerializedName("gastos")
    private Long mGastos;
    @SerializedName("id")
    private Long mId;
    @SerializedName("ingresos")
    private Long mIngresos;
    @SerializedName("saldoGeneral")
    private Long mSaldoGeneral;

    public Long getCuenta() {
        return mCuenta;
    }

    public void setCuenta(Long cuenta) {
        mCuenta = cuenta;
    }

    public Long getGastos() {
        return mGastos;
    }

    public void setGastos(Long gastos) {
        mGastos = gastos;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getIngresos() {
        return mIngresos;
    }

    public void setIngresos(Long ingresos) {
        mIngresos = ingresos;
    }

    public Long getSaldoGeneral() {
        return mSaldoGeneral;
    }

    public void setSaldoGeneral(Long saldoGeneral) {
        mSaldoGeneral = saldoGeneral;
    }

}
