
package com.example.evaluacionbankapp.models;

import com.google.gson.annotations.SerializedName;

public class Tarjeta {

    @SerializedName("estado")
    private String mEstado;
    @SerializedName("id")
    private Long mId;
    @SerializedName("nombre")
    private String mNombre;
    @SerializedName("saldo")
    private Long mSaldo;
    @SerializedName("tarjeta")
    private String mTarjeta;
    @SerializedName("tipo")
    private String mTipo;

    public String getEstado() {
        return mEstado;
    }

    public void setEstado(String estado) {
        mEstado = estado;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getNombre() {
        return mNombre;
    }

    public void setNombre(String nombre) {
        mNombre = nombre;
    }

    public Long getSaldo() {
        return mSaldo;
    }

    public void setSaldo(Long saldo) {
        mSaldo = saldo;
    }

    public String getTarjeta() {
        return mTarjeta;
    }

    public void setTarjeta(String tarjeta) {
        mTarjeta = tarjeta;
    }

    public String getTipo() {
        return mTipo;
    }

    public void setTipo(String tipo) {
        mTipo = tipo;
    }

}
