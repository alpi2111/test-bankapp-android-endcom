
package com.example.evaluacionbankapp.models;

import com.google.gson.annotations.SerializedName;

public class Movimiento {

    @SerializedName("descripcion")
    private String mDescripcion;
    @SerializedName("fecha")
    private String mFecha;
    @SerializedName("id")
    private Long mId;
    @SerializedName("monto")
    private String mMonto;
    @SerializedName("tipo")
    private String mTipo;

    public String getDescripcion() {
        return mDescripcion;
    }

    public void setDescripcion(String descripcion) {
        mDescripcion = descripcion;
    }

    public String getFecha() {
        return mFecha;
    }

    public void setFecha(String fecha) {
        mFecha = fecha;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMonto() {
        return mMonto;
    }

    public void setMonto(String monto) {
        mMonto = monto;
    }

    public String getTipo() {
        return mTipo;
    }

    public void setTipo(String tipo) {
        mTipo = tipo;
    }

}
