
package com.example.evaluacionbankapp.models;

import com.google.gson.annotations.SerializedName;

public class Cuentum {

    @SerializedName("cuenta")
    private Long mCuenta;
    @SerializedName("id")
    private Long mId;
    @SerializedName("nombre")
    private String mNombre;
    @SerializedName("ultimaSesion")
    private String mUltimaSesion;

    public Long getCuenta() {
        return mCuenta;
    }

    public void setCuenta(Long cuenta) {
        mCuenta = cuenta;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getNombre() {
        return mNombre;
    }

    public void setNombre(String nombre) {
        mNombre = nombre;
    }

    public String getUltimaSesion() {
        return mUltimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        mUltimaSesion = ultimaSesion;
    }

}
