
package com.example.evaluacionbankapp.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class TarjetasResponseModel {

    @SerializedName("tarjetas")
    private List<Tarjeta> mTarjetas;

    public List<Tarjeta> getTarjetas() {
        return mTarjetas;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        mTarjetas = tarjetas;
    }

}
