package com.example.evaluacionbankapp.providers;

import android.graphics.Outline;
import android.view.View;
import android.view.ViewOutlineProvider;

public class OutlineProvider extends ViewOutlineProvider {
    @Override
    public void getOutline(View view, Outline outline) {
        outline.setAlpha(0.20F);
        outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), (view.getWidth() / 40F));
    }
}