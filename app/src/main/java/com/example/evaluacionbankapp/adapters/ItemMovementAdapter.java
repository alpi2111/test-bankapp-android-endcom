package com.example.evaluacionbankapp.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.evaluacionbankapp.R;
import com.example.evaluacionbankapp.components.CardDetailLayout;
import com.example.evaluacionbankapp.models.Movimiento;
import com.example.evaluacionbankapp.models.Tarjeta;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class ItemMovementAdapter extends RecyclerView.Adapter<ItemMovementAdapter.ViewHolder> {

    private List<Movimiento> movimientos;
    private LayoutInflater mInflater;

    public ItemMovementAdapter(Context context, List<Movimiento> movimientos, Boolean listExpanded) {
        this.mInflater = LayoutInflater.from(context);
        if (!listExpanded && movimientos.size() > 3) {
            List<Movimiento> temp = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                temp.add(movimientos.get(i));
            }
            System.out.println(temp);
            this.movimientos = temp;
        } else {
            this.movimientos = movimientos;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_movement, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.i("pos", position + "");

        Movimiento movimiento = movimientos.get(position);

        holder.tvMovementTitle.setText(movimiento.getDescripcion());
        holder.tvMovementAmount.setText(String.format("$%s", movimiento.getMonto()));
        holder.tvMovementDate.setText(movimiento.getFecha());

        int color = holder.itemView.getResources().getColor(R.color.red);;
        if (movimiento.getTipo().equalsIgnoreCase("abono")) {
            color = holder.itemView.getResources().getColor(R.color.green);
        }
        holder.tvMovementAmount.setTextColor(color);
    }

    @Override
    public int getItemCount()  {
        return movimientos.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void updateMovements(List<Movimiento> movimientos, Boolean isClosing) {
        this.movimientos = movimientos;
        if (!isClosing) {
            notifyItemRangeInserted(3, movimientos.size());
        } else {
            List<Movimiento> temp = new ArrayList<>();
            for (int i = 0; i < 3; i++) {
                temp.add(movimientos.get(i));
            }
            System.out.println(temp);
            this.movimientos = temp;
            notifyItemRangeRemoved(movimientos.size(), this.movimientos.size());
            notifyDataSetChanged();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvMovementTitle;
        TextView tvMovementAmount;
        TextView tvMovementDate;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMovementTitle = itemView.findViewById(R.id.tv_title_movement);
            tvMovementAmount = itemView.findViewById(R.id.tv_movement_amount);
            tvMovementDate = itemView.findViewById(R.id.tv_movement_date);
        }
    }
}
