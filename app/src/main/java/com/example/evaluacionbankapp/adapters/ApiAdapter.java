package com.example.evaluacionbankapp.adapters;

import com.example.evaluacionbankapp.interfaces.ApiInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiAdapter {

    private static ApiInterface API_SERVICE;
    private static final String BASE_URL = "http://bankapp.endcom.mx/api/bankappTest/";

    public static ApiInterface getApiService() {
        if (API_SERVICE == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            API_SERVICE = retrofit.create(ApiInterface.class);
        }
        return API_SERVICE;
    }

}