package com.example.evaluacionbankapp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.evaluacionbankapp.R;
import com.example.evaluacionbankapp.components.CardDetailLayout;
import com.example.evaluacionbankapp.models.Saldo;
import com.example.evaluacionbankapp.models.Tarjeta;

import java.text.NumberFormat;
import java.util.List;

public class ItemCardAdapter extends RecyclerView.Adapter<ItemCardAdapter.ViewHolder> {

    private List<Tarjeta> tarjetas;
    private LayoutInflater mInflater;

    public ItemCardAdapter(Context context, List<Tarjeta> tarjetas) {
        this.mInflater = LayoutInflater.from(context);
        this.tarjetas = tarjetas;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.i("pos", position + "");

        Tarjeta tarjeta = tarjetas.get(position);
        holder.cardDetailLayout.setCardName(tarjeta.getNombre());

        holder.cardDetailLayout.setTypeCard(tarjeta.getTipo().substring(0, 1).toUpperCase() + tarjeta.getTipo().substring(1).toLowerCase());
        String saldoFormatted = NumberFormat.getInstance().format(tarjeta.getSaldo());
        String cardNumber = tarjeta.getTarjeta();
        StringBuilder numberTemp = new StringBuilder();
        System.out.println(cardNumber.length());
        for (int i = 1; i <= cardNumber.length(); i++) {
            if (i % 4 == 0) {
                numberTemp.append(cardNumber.substring(i - 4, i)).append(" ");
            }
        }
        holder.cardDetailLayout.setCardNumber(numberTemp.toString().trim());
        holder.cardDetailLayout.setAmount(String.format("$%s.00", saldoFormatted));
        if (tarjeta.getEstado().equalsIgnoreCase("activa")) {
            holder.cardDetailLayout.setIsCardActive(true);
        } else {
            holder.cardDetailLayout.setIsCardActive(false);
        }
    }

    @Override
    public int getItemCount()  {
        return tarjetas.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CardDetailLayout cardDetailLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cardDetailLayout = itemView.findViewById(R.id.cdl_card);
        }
    }
}
