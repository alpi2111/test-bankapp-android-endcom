package com.example.evaluacionbankapp.controllers;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.evaluacionbankapp.adapters.ApiAdapter;
import com.example.evaluacionbankapp.interfaces.ApiInterface;
import com.example.evaluacionbankapp.models.CuentaResponseModel;
import com.example.evaluacionbankapp.models.MovementsResponseModel;
import com.example.evaluacionbankapp.models.SaldosResponseModel;
import com.example.evaluacionbankapp.models.TarjetasResponseModel;

import java.util.Observable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityController extends Observable {
    private CuentaResponseModel cuentaResponseModel;
    private SaldosResponseModel saldosResponseModel;
    private TarjetasResponseModel tarjetasResponseModel;
    private MovementsResponseModel movimientosResponseModel;

    public MainActivityController() {
        ApiInterface apiInterface = ApiAdapter.getApiService();
        Call<CuentaResponseModel> callAccount = apiInterface.getAccount();
        Call<SaldosResponseModel> callSaldos = apiInterface.getSaldos();
        Call<TarjetasResponseModel> callTarjetas = apiInterface.getTarjetas();
        Call<MovementsResponseModel> callMovimientos = apiInterface.getMovimientos();

        callAccount.enqueue(new Callback<CuentaResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<CuentaResponseModel> call, @NonNull Response<CuentaResponseModel> response) {
                assert response.body() != null;
                Log.i("responseSuccess", response.body().getCuenta().toString());
                updateAccountInfo(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<CuentaResponseModel> call, @NonNull Throwable t) {
                Log.e("responseError", t.getMessage());
            }
        });

        callSaldos.enqueue(new Callback<SaldosResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<SaldosResponseModel> call, @NonNull Response<SaldosResponseModel> response) {
                assert response.body() != null;
                Log.i("responseSuccess", response.body().getSaldos().toString());
                updateSaldos(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<SaldosResponseModel> call, @NonNull Throwable t) {
                Log.e("responseError", t.getMessage());
            }
        });

        callTarjetas.enqueue(new Callback<TarjetasResponseModel>() {
            @Override
            public void onResponse(@NonNull Call<TarjetasResponseModel> call, @NonNull Response<TarjetasResponseModel> response) {
                assert response.body() != null;
                Log.i("responseSuccess", response.body().getTarjetas().toString());
                updateTarjetas(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<TarjetasResponseModel> call, @NonNull Throwable t) {
                Log.e("responseError", t.getMessage());
            }
        });

        callMovimientos.enqueue(new Callback<MovementsResponseModel>() {
            @Override
            public void onResponse(Call<MovementsResponseModel> call, Response<MovementsResponseModel> response) {
                Log.i("responseSuccess", response.body().getMovimientos().toString());
                updateMovimientos(response.body());
            }

            @Override
            public void onFailure(Call<MovementsResponseModel> call, Throwable t) {
                Log.e("responseError", t.getMessage());
            }
        });
    }

    public void updateAccountInfo(CuentaResponseModel body) {
        cuentaResponseModel = body;
        setChanged();
        notifyObservers();
    }

    public CuentaResponseModel getAccountInfo() {
        return cuentaResponseModel;
    }

    public void updateSaldos(SaldosResponseModel body) {
        saldosResponseModel = body;
        setChanged();
        notifyObservers();
    }

    public SaldosResponseModel getSaldos() {
        return saldosResponseModel;
    }

    public void updateTarjetas(TarjetasResponseModel body) {
        tarjetasResponseModel = body;
        setChanged();
        notifyObservers();
    }

    public TarjetasResponseModel getTarjetas() {
        return tarjetasResponseModel;
    }

    public void updateMovimientos(MovementsResponseModel body) {
        movimientosResponseModel = body;
        setChanged();
        notifyObservers();
    }

    public MovementsResponseModel getMovimientos() {
        return movimientosResponseModel;
    }
}
