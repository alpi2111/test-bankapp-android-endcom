package com.example.evaluacionbankapp.interfaces;

import com.example.evaluacionbankapp.models.CuentaResponseModel;
import com.example.evaluacionbankapp.models.MovementsResponseModel;
import com.example.evaluacionbankapp.models.SaldosResponseModel;
import com.example.evaluacionbankapp.models.TarjetasResponseModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("cuenta")
    Call<CuentaResponseModel> getAccount();

    @GET("saldos")
    Call<SaldosResponseModel> getSaldos();

    @GET("tarjetas")
    Call<TarjetasResponseModel> getTarjetas();

    @GET("movimientos")
    Call<MovementsResponseModel> getMovimientos();
}