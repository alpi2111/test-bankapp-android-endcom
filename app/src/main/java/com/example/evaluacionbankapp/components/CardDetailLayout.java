package com.example.evaluacionbankapp.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.evaluacionbankapp.R;
import com.example.evaluacionbankapp.providers.OutlineProvider;

public class CardDetailLayout extends LinearLayout {
    private final ImageView mImageView;
    private final TextView mTvCardName;
    private final TextView mTvTypeCard;
    private final TextView mTvAmount;
    private final TextView mTvCardNumber;
    private final TextView mTvActiveLabel;
    private Boolean isCardActive;

    public void setIsCardActive(Boolean isCardActive) {
        this.isCardActive = isCardActive;
        if (isCardActive) {
            setActiveLabel("Activa");
            updateImageCardActive(R.drawable.ic_card_active);
        } else {
            setActiveLabel("Inactiva");
            updateImageCardActive(R.drawable.ic_card_inactive);
        }
    }

    public void setCardName(String str) {
        this.mTvCardName.setText(str);
    }

    private void setActiveLabel(String str) {
        this.mTvActiveLabel.setText(str);
    }

    private void updateImageCardActive(int id) {
        mImageView.setImageResource(id);
    }

    public void setTypeCard(String str) {
        this.mTvTypeCard.setText(str);
    }

    public void setAmount(String str) {
        this.mTvAmount.setText(str);
    }

    public void setCardNumber(String str) {
        this.mTvCardNumber.setText(str);
    }

    public CardDetailLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CardDetailLayout,
                0, 0);
        View view = LayoutInflater.from(context).inflate(R.layout.card_detail_layout, this, true);

        try {
            isCardActive = a.getBoolean(R.styleable.CardDetailLayout_is_card_active, false);
            mImageView = view.findViewById(R.id.iv_card);
            mTvCardName = view.findViewById(R.id.tv_card_name);
            mTvCardNumber = view.findViewById(R.id.tv_card_number);
            mTvActiveLabel = view.findViewById(R.id.tv_active_label);
            mTvTypeCard = view.findViewById(R.id.tv_type_card);
            mTvAmount = view.findViewById(R.id.tv_amount);
            mImageView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            if (isCardActive) {
                mImageView.setImageResource(R.drawable.ic_card_active);
            } else {
                mImageView.setImageResource(R.drawable.ic_card_inactive);
            }
            mImageView.setOutlineProvider(new OutlineProvider());
        } finally {
            a.recycle();
        }
    }


}