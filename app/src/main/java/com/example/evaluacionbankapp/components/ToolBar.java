package com.example.evaluacionbankapp.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.evaluacionbankapp.R;

public class ToolBar extends LinearLayout {
    private final ImageView mImageViewToolBar;
    private final ImageView mImageViewBack;
    private final TextView mToolBarTitle;
    private final String toolBarTitle;
    private final Boolean isArrowButtonVisible;

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mImageViewBack.setOnClickListener(onClickListener);
    }

    public ToolBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.ToolBar,
                0, 0);

        View view = LayoutInflater.from(context).inflate(R.layout.tool_bar, this, true);

        try {
            // imageDrawable = a.getDrawable(R.styleable.ToolBar_image_drawable);
            toolBarTitle = a.getString(R.styleable.ToolBar_title_text);
            isArrowButtonVisible = a.getBoolean(R.styleable.ToolBar_showBackArrow, false);
        } finally {
            a.recycle();
        }

        setOrientation(VERTICAL);
        mImageViewToolBar = view.findViewById(R.id.iv_toolbar_image);
        mImageViewBack = view.findViewById(R.id.iv_arrow_back);
        mToolBarTitle = view.findViewById(R.id.tv_toolbar_title);

        if (isArrowButtonVisible) {
            mImageViewBack.setVisibility(VISIBLE);
        }

        if (toolBarTitle != null) {
            mImageViewToolBar.setVisibility(GONE);
            mToolBarTitle.setText(toolBarTitle);
            mToolBarTitle.setVisibility(VISIBLE);
        } else {
            mImageViewToolBar.setVisibility(VISIBLE);
            // mImageViewToolBar.setImageDrawable(imageDrawable);
        }

    }
}