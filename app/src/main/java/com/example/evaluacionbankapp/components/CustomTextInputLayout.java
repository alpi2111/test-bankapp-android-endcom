package com.example.evaluacionbankapp.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.evaluacionbankapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class CustomTextInputLayout extends LinearLayout {

    private final TextInputLayout tilCustom;
    private final TextInputEditText tiedCustom;
    private final String hintText;

    public TextInputEditText getEditText() {
        return (TextInputEditText) tilCustom.getEditText();
    }

    public CustomTextInputLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomTextInputLayout,
                0, 0);
        View view = LayoutInflater.from(context).inflate(R.layout.custom_text_input_layout, this, true);
        try {
            tiedCustom = view.findViewById(R.id.tiet_custom);
            tilCustom = view.findViewById(R.id.til_custom);
            hintText = a.getString(R.styleable.CustomTextInputLayout_hint);
            tilCustom.setHint(hintText);
            tiedCustom.setInputType(a.getInt(R.styleable.CustomTextInputLayout_input_type, R.attr.text));
        } finally {
            a.recycle();
        }

    }
}