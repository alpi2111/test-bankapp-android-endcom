package com.example.evaluacionbankapp.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

import com.example.evaluacionbankapp.R;

public class UnderlinedTextView extends LinearLayout {
    private final TextView mUnderlinedTextView;
    private final String text;
    private final int color;

    public UnderlinedTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.UnderlinedTextView,
                0, 0);
        View view = LayoutInflater.from(context).inflate(R.layout.underlined_text_view, this, true);
        mUnderlinedTextView = view.findViewById(R.id.tv_underlined_text);
        try {
            text = a.getString(R.styleable.UnderlinedTextView_text);
            color = a.getColor(R.styleable.UnderlinedTextView_text_color, getResources().getColor(R.color.aqua));
            mUnderlinedTextView.setText(text);
            mUnderlinedTextView.setPaintFlags(mUnderlinedTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            mUnderlinedTextView.setTextColor(color);
        } finally {
            a.recycle();
        }

    }
}